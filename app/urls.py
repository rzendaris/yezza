from django.conf.urls import url
from django.urls import include

urlpatterns = [
    # API endpoints V1
    url(r'^api/v1/',
        include([
            url(r'^auth/', include(('service.user.v1.urls', 'service.user'), namespace='user-v1')),
            url(r'^note/', include(('service.note.v1.urls', 'service.note'), namespace='note-v1')),
        ])
    ),
    # API endpoints V2
]
