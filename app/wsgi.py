import os
import config

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', config.SERVER)

application = get_wsgi_application()
