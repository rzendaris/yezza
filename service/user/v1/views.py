from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.views import APIView
from rest_framework import status

from service.user.models import User
from service.user.v1.serializers import UserLoginSerializer, UserRegisterSerializer
from service.base.functions import api_response


class LoginView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = UserLoginSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            return api_response(code='00', data=serializer.data, message='Success login user')
        else:
            return api_response(code='01', data=serializer.errors, message='Failed login user',
                                http_code=status.HTTP_401_UNAUTHORIZED)


class RegisterView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = UserRegisterSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            validated_data = serializer.data
            user = User.objects.create(
                name=validated_data['email'],
                email=validated_data['email'],
                is_active=True
            )
            user.set_password(validated_data['password'])
            user.save()
            return api_response(code='00', data=serializer.data, message='Success register user')
        else:
            return api_response(code='01', data=serializer.errors, message='Failed register user',
                                http_code=status.HTTP_401_UNAUTHORIZED)
