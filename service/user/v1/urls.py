from django.urls import path
from . import views

app_name = 'user-v1'

urlpatterns = [
    path('login/', views.LoginView.as_view()),
    path('register/', views.RegisterView.as_view()),
]
