from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager
)
from django.db import models


class UserManager(BaseUserManager):
    def create_user(self, email, password=None):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    name = models.CharField(max_length=255)
    email = models.CharField(max_length=255, unique=True)
    password = models.TextField()
    is_active = models.BooleanField()
    notion_access_token = models.CharField(max_length=255, blank=True)
    notion_bot_id = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    USERNAME_FIELD = 'email'
    objects = UserManager()

    class Meta:
        managed = True
        db_table = 'user'
