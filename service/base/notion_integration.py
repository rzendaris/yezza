import base64

import requests

import config


class NotionIntegration:

    @staticmethod
    def send_get(url, headers=None):
        r = requests.get('{0}{1}'.format(config.NOTION_URL, url), headers=headers)
        r.raise_for_status()
        return r.json()

    @staticmethod
    def send_post(url, data, headers=None):
        r = requests.post('{0}{1}'.format(config.NOTION_URL, url), json=data, headers=headers)
        r.raise_for_status()
        return r.json()

    @staticmethod
    def send_put(url, data, headers=None):
        r = requests.put('{0}{1}'.format(config.NOTION_URL, url), json=data, headers=headers)
        r.raise_for_status()
        return r.json()

    @staticmethod
    def send_delete(url, headers=None):
        r = requests.delete('{0}{1}'.format(config.NOTION_URL, url), headers=headers)
        r.raise_for_status()
        return r.json()

    @staticmethod
    def get_access_token(code):
        body = {
            "grant_type": "authorization_code",
            "code": code,
            "redirect_uri": config.NOTION_REDIRECT_URL
        }
        combined_code = "{0}:{1}".format(config.NOTION_CLIENT_ID, config.NOTION_CLIENT_SECRET)
        basic_token = base64.b64encode(combined_code.encode('ascii'))
        headers = {
            "Content-Type": "application/json",
            "Authorization": "Basic {0}".format(basic_token.decode('ascii')),
        }
        return NotionIntegration.send_post("v1/oauth/token", body, headers)

    @staticmethod
    def get_databases(access_token):
        body = {
            "query": "",
            "sort": {
                "direction": "descending",
                "timestamp": "last_edited_time"
            },
            "filter": {
                "value": "database",
                "property": "object"
            },
        }
        headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer {0}".format(access_token),
            "Notion-Version": "2022-02-22",
        }
        return NotionIntegration.send_post("v1/search", body, headers)

    @staticmethod
    def create_note(access_token, request_data):
        body = {
            "parent": {
                "type": "database_id",
                "database_id": request_data['database_id']
            },
            "Name": {
                "title": [{
                    "text": {
                        "content": request_data['title']
                    }
                }],
            },
            "Description": {
                "rich_text": [{
                    "text": {
                        "content": request_data['description']
                    }
                }],
            },
        }
        headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer {0}".format(access_token),
            "Notion-Version": "2022-02-22",
        }
        return NotionIntegration.send_post("v1/pages/", body, headers)
