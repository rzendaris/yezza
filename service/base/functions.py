from rest_framework.response import Response
from rest_framework import status


def api_response(code, data=[], message=None, http_code=status.HTTP_200_OK):
    response_maps = {
        '00': {'code': code, 'status': True, 'message': message, 'data': data},
        '01': {'code': code, 'status': False, 'message': message, 'data': data},
        '50': {'code': code, 'status': False, 'message': 'internal server error', 'data': data},
    }
    response = response_maps[code]
    return Response(response, status=http_code)
