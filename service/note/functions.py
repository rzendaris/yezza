from datetime import datetime, timedelta

from service.note.models import Note, NoteAttachment


def create_note(user, text, files):
    note = Note.objects.create(
        user=user,
        text=text,
        remind_at=datetime.today() + timedelta(minutes=2)
    )
    for file in files:
        NoteAttachment.objects.create(
            note_field=note,
            attachment=file
        )

    return note
