from django.db import models

from service.base.models import BaseModel
from service.user.models import User


class Note(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.TextField()
    remind_at = models.DateTimeField()


class NoteAttachment(BaseModel):
    note_field = models.ForeignKey(Note, on_delete=models.CASCADE)
    attachment = models.FileField()
