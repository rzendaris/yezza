from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.views import APIView

from service.base.functions import api_response
from service.note.functions import create_note
from service.note.models import Note
from service.note.v1.serializers import NoteSerializer, FormNoteSerializer


class NotesView(APIView):
    allowed_methods = ('GET',)
    permission_classes = (AllowAny,)

    def get(self, request):
        notes = Note.objects.all()
        serializer = NoteSerializer(notes, many=True)
        return api_response(code='00', data=serializer.data, message='get data success')


class NotesFetchView(APIView):
    allowed_methods = ('GET',)
    permission_classes = (AllowAny,)

    def get(self, request, note_id):
        note = Note.objects.filter(id=note_id).first()
        if note:
            serializer = NoteSerializer(note)
            return api_response(code='00', data=serializer.data, message='get data success')
        else:
            return api_response(code='01', data=[], message='Note ID not Found',
                                http_code=status.HTTP_404_NOT_FOUND)


class CreateNoteView(APIView):
    allowed_methods = ('POST',)
    permission_classes = (IsAuthenticated,)
    serializer_class = FormNoteSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            validated_data = serializer.data
            note = create_note(request.user, validated_data['text'], request.FILES.getlist('attachment'))
            serializer = NoteSerializer(note)
            return api_response(code='00', data=serializer.data, message='Note Created',
                                http_code=status.HTTP_201_CREATED)
        else:
            return api_response(code='01', data=serializer.errors, message='Failed create Note',
                                http_code=status.HTTP_400_BAD_REQUEST)


class EditNoteView(APIView):
    allowed_methods = ('PUT',)
    permission_classes = (IsAuthenticated,)
    serializer_class = FormNoteSerializer

    def put(self, request, note_id):
        note = Note.objects.filter(id=note_id, user=request.user).first()
        if note:
            serializer = self.serializer_class(data=request.data)
            if serializer.is_valid():
                validated_data = serializer.data
                note.text = validated_data['text']
                note.save()
                serializer = NoteSerializer(note)
                return api_response(code='00', data=serializer.data, message='Note Updated')
            else:
                return api_response(code='01', data=serializer.errors, message='Failed Update Note',
                                    http_code=status.HTTP_400_BAD_REQUEST)
        else:
            return api_response(code='01', data=[], message="Note ID not Found or User isn't an Author",
                                http_code=status.HTTP_404_NOT_FOUND)


class DeleteNoteView(APIView):
    allowed_methods = ('DELETE',)
    permission_classes = (IsAuthenticated,)

    def delete(self, request, note_id):
        note = Note.objects.filter(id=note_id, user=request.user).first()
        if note:
            note.delete()
            return api_response(code='00', data={}, message='Note Deleted')
        else:
            return api_response(code='01', data=[], message="Note ID not Found or User isn't an Author",
                                http_code=status.HTTP_404_NOT_FOUND)
