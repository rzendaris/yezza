from rest_framework import serializers


class FormNotionSerializer(serializers.Serializer):
    database_id = serializers.CharField(required=True)
    title = serializers.CharField(max_length=255)
    description = serializers.CharField(max_length=255)


class NotionDatabaseSerializer(serializers.Serializer):
    id = serializers.CharField(max_length=255)
    database_name = serializers.SerializerMethodField()

    def get_database_name(self, obj):
        return obj['title'][0]['plain_text']
