from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.views import APIView

import config
from service.base.functions import api_response
from service.base.notion_integration import NotionIntegration
from service.note.v1.notion.notion_serializers import NotionDatabaseSerializer, FormNotionSerializer
from service.user.models import User


class NotionAuthorizeView(APIView):
    allowed_methods = ('GET',)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        message = "User Unauthorized, please click link below."
        notion_url = "{0}v1/oauth/authorize?owner=user&client_id={1}&redirect_uri={2}&response_type=code&state='{3}'" \
            .format(config.NOTION_URL,
                    config.NOTION_CLIENT_ID,
                    config.NOTION_REDIRECT_URL,
                    request.user.id)
        if request.user.notion_access_token:
            message = 'User Authorized'
            notion_url = ''
        return api_response(code='00', data=notion_url, message=message,
                            http_code=status.HTTP_200_OK)


class NotionCallbackView(APIView):
    allowed_methods = ('GET',)
    permission_classes = (AllowAny,)

    def get(self, request):
        code = request.GET.get('code', '')
        user_id_str = request.GET.get('state', '')
        if code and user_id_str:
            notion_response = NotionIntegration.get_access_token(code)
            if 'access_token' in notion_response:
                user_id = int(user_id_str.replace("'", ""))
                user = User.objects.filter(id=int(user_id)).first()
                user.notion_access_token = notion_response['access_token']
                user.notion_bot_id = notion_response['bot_id']
                user.save()

        return api_response(code='00', data=[], message='Callback Success',
                            http_code=status.HTTP_200_OK)


class NotionDatabaseView(APIView):
    allowed_methods = ('GET',)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        if request.user.notion_access_token:
            databases = NotionIntegration.get_databases(request.user.notion_access_token)
            serializer = NotionDatabaseSerializer(databases['results'], many=True)
            return api_response(code='00', data=serializer.data, message='get data success')
        else:
            return api_response(code='01', data=[], message='User unauthorized',
                                http_code=status.HTTP_400_BAD_REQUEST)


class NotionCreateNoteView(APIView):
    allowed_methods = ('POST',)
    permission_classes = (IsAuthenticated,)
    serializer_class = FormNotionSerializer

    def post(self, request):
        if request.user.notion_access_token:
            form_serializer = self.serializer_class(data=request.data)
            if form_serializer.is_valid():
                validated_data = form_serializer.data
                created = NotionIntegration.create_note(request.user.notion_access_token, validated_data)
                return api_response(code='00', data=[], message='Notion Note Created',
                                    http_code=status.HTTP_201_CREATED)
            else:
                return api_response(code='01', data=form_serializer.errors, message='Failed create Note',
                                    http_code=status.HTTP_400_BAD_REQUEST)
        else:
            return api_response(code='01', data=[], message='User unauthorized',
                                http_code=status.HTTP_400_BAD_REQUEST)
