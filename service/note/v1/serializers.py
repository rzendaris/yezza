from rest_framework import serializers

from service.note.models import Note, NoteAttachment


class FormNoteSerializer(serializers.Serializer):
    text = serializers.CharField(required=True)
    attachment = serializers.FileField(required=False)


class NoteSerializer(serializers.HyperlinkedModelSerializer):
    attachment = serializers.SerializerMethodField()

    class Meta:
        model = Note
        fields = ['id', 'text', 'attachment']

    def get_attachment(self, obj):

        attachments = NoteAttachment.objects.filter(note_field=obj).all()
        return NoteAttachmentSerializer(attachments, many=True).data


class NoteAttachmentSerializer(serializers.HyperlinkedModelSerializer):
    url = serializers.SerializerMethodField()

    class Meta:
        model = NoteAttachment
        fields = ['url']

    def get_url(self, obj):
        return obj.attachment.url
