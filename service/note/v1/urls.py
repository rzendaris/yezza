from django.urls import path
from django.conf.urls import url
from . import views
from .notion import notion_views

app_name = 'note-v1'

urlpatterns = [
    path('', views.NotesView.as_view()),
    url(r'^create/', views.CreateNoteView.as_view()),
    url(r'^fetch/(?P<note_id>[0-9]+)/', views.NotesFetchView.as_view()),
    url(r'^edit/(?P<note_id>[0-9]+)/', views.EditNoteView.as_view()),
    url(r'^delete/(?P<note_id>[0-9]+)/', views.DeleteNoteView.as_view()),
    url(r'^notion/authorize/', notion_views.NotionAuthorizeView.as_view()),
    url(r'^notion/callback/', notion_views.NotionCallbackView.as_view()),
    url(r'^notion/database/', notion_views.NotionDatabaseView.as_view()),
    url(r'^notion/add-note/', notion_views.NotionCreateNoteView.as_view()),
]
