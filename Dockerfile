FROM python:3.6-slim
MAINTAINER Rafi Zendaris <rzendaris@gmail.com>

ENV PROJECT_ROOT /app
WORKDIR $PROJECT_ROOT

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY . .
CMD gunicorn -c gunicorn.conf app.wsgi:application --reload